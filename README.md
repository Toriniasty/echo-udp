# Simple UDP echo server

## Parameters

```bash
  -l, --listen string   Listen address. Default: 0.0.0.0 (default "0.0.0.0")
  -p, --port int        Listen port. Default: 9300 (default 9300)
```

## How to use

### Start the server

By either:

1) Compiling it and running

```bash
$ go build
$ ./echo-udp
2021/07/12 18:08:53 [UDP] Listening on 0.0.0.0:9300. Hostname: laptopix
```

2) Just running the code

```bash
$ go run echo-udp.go 
2021/07/12 18:08:53 [UDP] Listening on 0.0.0.0:9300. Hostname: laptopix
```

### Connect

```bash
$ echo zzz | nc -u -W 1 localhost 9300
[UDP] Incoming connection from: ::1:42175, My hostname: laptopix, Message: zzz
```
