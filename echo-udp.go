package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	flag "github.com/spf13/pflag"
)

var listen string
var port int
var hostname string
var err error

func init() {
	flag.StringVarP(&listen, "listen", "l", "0.0.0.0", "Listen address. Default: 0.0.0.0")
	flag.IntVarP(&port, "port", "p", 9300, "Listen port. Default: 9300")
	flag.Parse()

	hostname, err = os.Hostname()
	if err != nil {
		panic(err)
	}
}

func main() {
	// Listen for incoming connections.
	l, err := net.ListenUDP("udp", &net.UDPAddr{
		Port: port,
		IP:   net.ParseIP(listen),
	})
	if err != nil {
		log.Println("Error listening:", err.Error())
		os.Exit(1)
	}

	// Close the listener when the application closes.
	defer l.Close()

	log.Printf("[UDP] Listening on %s:%d. Hostname: %s", listen, port, hostname)
	for {
		// Listen for an incoming connection.
		buf := make([]byte, 1024)
		rlen, remote, err := l.ReadFromUDP(buf[:])
		if err != nil {
			log.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}

		// Handle connections in a new goroutine.
		go handleRequest(buf, rlen, remote, l)
	}
}

// Handles incoming requests.
func handleRequest(buf []byte, rlen int, remote *net.UDPAddr, conn *net.UDPConn) {
	// Read the incoming connection into the buffer.
	message := strings.TrimRight(string(buf[:rlen]), "\n")

	// Prepare log messages
	logStringClient := fmt.Sprintf("[UDP] Incoming connection from: %s:%d, My hostname: %s, Message: %s\n", remote.IP, remote.Port, hostname, message)
	logStringServer := fmt.Sprintf("[UDP] Incoming connection from: %s:%d, Message: %s\n", remote.IP, remote.Port, message)
	log.Print(logStringServer)

	// Send a response back to person contacting us.
	_, err = conn.WriteTo([]byte(logStringClient), remote)
	if err != nil {
		log.Printf("net.WriteTo() error: %s\n", err)
	}
}
