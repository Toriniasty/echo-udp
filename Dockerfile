FROM golang:latest as builder
WORKDIR /go/src/app
COPY . .
RUN CGO_ENABLED=0 go build -o echoserver-udp .

FROM alpine:latest
COPY --from=builder /go/src/app/echoserver-udp /usr/bin/echoserver-udp

ENTRYPOINT ["/usr/bin/echoserver-udp"]